import TodoList from '../../classes/todo/DoList'

export default class TodoController {
  constructor() {
    //this.todoItem = '';
    this.todoList = new TodoList();
  }

  addTodo() {

    this.todoList.create(this.todoItem);
  }

}

TodoController.$inject = [];
