import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './todo.routes';
import TodoController from './todo.controller';


export default angular.module('app.todo', [uirouter])
  .config(routing)
  .controller('TodoController', TodoController)
  .name;
