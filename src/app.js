import 'bootstrap/dist/css/bootstrap.css';
import './style/app.css';

import angular from 'angular';
import uirouter from 'angular-ui-router';
import routing from './app.config';
import home from './features/home';
import todo from './features/todo';

angular.module('app', [uirouter, home,todo])
  .config(routing);
