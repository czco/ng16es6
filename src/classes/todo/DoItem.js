class DoItem {
  constructor(content, done = false, timeStamp = DoItem.timeStamp()) {
    this.content = content;
    this.done = done;
    this.timeStamp = timeStamp;
  }

  static timeStamp() {
    return Date.now().toString();
  }
}

export default DoItem;
// "default" because there is one main thing a module exports.
