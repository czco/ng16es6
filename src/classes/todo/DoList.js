import DoItem from './DoItem';

class DoList {
  constructor() {
    this.items = [];
    this.length = 0;
  }

  create(content, done = false) {
    let doItem = new DoItem(content, done);
    this.items.push(doItem);
    this.length++;
  }


  delete(todo) {
    let arr = this.items;
    for(var i = 0;arr.length; i++){
      if (arr[i].timeStamp === todo.timeStamp) {
        arr.splice(i, 1);
      }
    }
    this.length--;
  }

}

export default DoList;
