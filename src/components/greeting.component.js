import angular from 'angular';

var greeting = {
  bindings: {
    name: '='
  },
  template: '<h1>Hello, {{$ctrl.name}}</div>'
}

export default angular.module('components.greeting', [])
  .component('greeting', greeting)
  .name; //external modules always return their name
